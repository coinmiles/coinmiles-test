# Coinmiles Backend Test

The aim of this test is to perform the following:

- Fetch test transactions from two REST API endpoints using Axios
- Map the returned transaction arrays to a consistent transaction format
- Create the Mongoose schema/model
- Save the mapped transactions to the test database
- Update the transactions in the database
- Fetch the saved & updated transactions from the test database

### Dependencies

- Axios
- Mongoose
- Typescript

### Endpoints

There are two REST endpoints to fetch the transactions, with the following details:

- Method: `POST`
- Data: An object with the following field:
  - **mode**: one of "create", "update", "complete"

Trim the returned network transactions arrays to the first 30 results each.

### Mapping Data

There are two different data shapes that will be returned from the endpoints, with fields that correspond to the ITransaction interface as follows:

###### INetworkTransactionOne

orderNumber -> orderId\
programId -> advertiserId\
programName -> advertiserName\
subTracking -> userId\
merchantValue -> totalSaleAmount\
commission -> totalCommission\
postedDate *or* eventDate *or* clickDate -> postedDate

(For **postedDate** use the first of the three possible fields that is available, in the given order)

###### INetworkTransactionTwo

Id -> orderId\
CampaignId -> advertiserId\
CampaignName -> advertiserName\
SubId1 -> userId\
Amount -> totalSaleAmount\
Payout -> totalCommission\
CreationDate -> postedDate

### MongoDB

- The client used is Mongoose and the connection will need to be initialized and the mapped ITransaction objects saved to the database.

- Database user and password will be sent via email.

- The schema fields should be filled in and a unique index placed on the **orderId** field.

- The create method should prevent attempting to write duplicate **orderId**s to the dataase.

- The update methods should update the transactions according to the following conditions:
  - Updates all transactions with a **postedDate** later than `January 1, 2021` to `"complete"`
  - Updates all transactions with a **totalCommission** of `0` to `"invalid"`

### Notes

- Use `Node v14`
- Use `yarn`
- When project is ready, run the script `yarn run-test`
- Push your solution to the repo as a feature branch

## Evaluation

- The project should build using the `yarn build` command
- The fetched transactions should be created in the test database with the correct fields
- There should be no more than 60 transactions in the database
- The transactions should be updated according to the given criteria
- All variables and return types should be typed as much as possible
- Code should be neat, clear, well formatted and have reasonable comments
- Bonus: clearly typed variables, params and return types and use of ES6 patterms
