/* DO NOT MODIFY THIS FILE */
import mongoose from "mongoose";
import { Service } from "./service";

(async () => {
  const service = new Service();
  await service.fetchAndSaveTransactions();
  await service.updateTransactionsToComplete();
  await service.updateTransactionsToInvalid();
  const savedTransactions = await service.getSavedTransactions();
  if (savedTransactions?.length) {
    if (savedTransactions.length <= 60) {
      console.log(
        `Success! Fetched ${savedTransactions.length} saved transactions from the database.`,
      );
    }
    if (
      savedTransactions.some(({ status }) => status === "complete") &&
      savedTransactions.some(({ status }) => status === "invalid")
    ) {
      console.log(
        `Success! Transactions were updated to complete and invalid.`,
      );
    }
  } else {
    console.log("Transactions not found in database, please try again.")
  }
  await mongoose.disconnect();
})();
/* DO NOT MODIFY THIS FILE */
