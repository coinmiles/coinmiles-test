export interface INetworkTransactionOne {
  categoryName: string;
  programName: string;
  domainName: string;
  subTracking: string;
  orderNumber: string;
  currency: string;
  productName: string;
  programId: string;
  subId1: string;
  subId2: string;
  subId3: string;
  subId4: string;
  subId5: string;
  orderStatus: string;
  legacyId: number;
  flX_SalesId: number;
  categoryId: number;
  productId: number;
  domainId: number;
  accountId: number;
  tracking: number;
  clickId: number;
  commission: number;
  merchantValue: number;
  postedDate: Date;
  clickDate: Date;
  eventDate: Date;
  lockingDate: string;
  modifiedDate: Date;
}

export interface INetworkTransactionTwo {
  Id: string;
  CampaignId: string;
  CampaignName: string;
  ActionTrackerId: string;
  ActionTrackerName: string;
  EventCode: string;
  AdId: string;
  Payout: string;
  DeltaPayout: string;
  IntendedPayout: string;
  Amount: string;
  DeltaAmount: string;
  IntendedAmount: string;
  Currency: string;
  ReferringType: string;
  ReferringDomain: string;
  PromoCode: string;
  Oid: string;
  CustomerArea: string;
  CustomerCity: string;
  CustomerRegion: string;
  CustomerCountry: string;
  SubId1: string;
  SubId2: string;
  SubId3: string;
  SharedId: string;
  Uri: string;
  ClearedDate: string;
  State: string;
  ReferringDate: Date;
  EventDate: Date;
  CreationDate: Date;
  LockingDate: string;
}

export interface ITransaction {
  orderId: string;
  advertiserId: string;
  advertiserName: string;
  userId: string;
  postedDate: Date;
  totalSaleAmount: number;
  totalCommission: number;
  status: "pending" | "complete" | "invalid";
}
