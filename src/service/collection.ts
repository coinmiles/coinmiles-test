import mongoose from "mongoose";

import { ITransaction } from "../models";

const transactionsSchema = new mongoose.Schema(
  {
    // Fill in the rest of Schema fields
    status: {
      type: String,
      enum: Object.values(["pending", "complete", "invalid"]),
    },
  },
  { timestamps: true },
);

const collection = mongoose.model<ITransaction>(
  "Transactions",
  transactionsSchema,
);

export { collection as Collection };
