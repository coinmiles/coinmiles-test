import mongoose from "mongoose";
import axios from "axios";

import { Collection } from "./collection";
import {
  INetworkTransactionOne,
  INetworkTransactionTwo,
  ITransaction,
} from "../models";

// Network Transaction Endpoints
enum ETransactionEndpoints {
  TEST_ENDPOINT_1 = "https://855dgl99sf.execute-api.ca-central-1.amazonaws.com/dev/system-rest-api/test-api/v1/flex",
  TEST_ENDPOINT_2 = "https://855dgl99sf.execute-api.ca-central-1.amazonaws.com/dev/system-rest-api/test-api/v1/impact",
}

// Mongoose Connection URI
const testDbURI =
  "mongodb+srv://{dbUser}:{password}@coinmiles-test.qihuh.mongodb.net/test";

export class Service {
  /* ---- Public Methods ---- */
  // Call all private methods from this method
  public async fetchAndSaveTransactions() {}

  // Update transactions afer they have been created
  public async updateTransactionsToComplete(): Promise<void> {}
  public async updateTransactionsToInvalid(): Promise<void> {}

  // Fetch transactions from database afer they have been created and updated
  public async getSavedTransactions(): Promise<ITransaction[]> {}

  /* ---- Private Methods ---- */
  private async initMongooseConnection(): Promise<void> {}

  private async getTransactionsOne(): Promise<INetworkTransactionOne[]> {}

  private async getTransactionsTwo(): Promise<INetworkTransactionTwo[]> {}

  private async mapTransactionsOne(
    networkTransactions: INetworkTransactionOne[],
  ): Promise<ITransaction[]> {}

  private async mapTransactionsTwo(
    networkTransactions: INetworkTransactionTwo[],
  ): Promise<ITransaction[]> {}

  private async createTransactions(transactions: ITransaction[]) {}
}
